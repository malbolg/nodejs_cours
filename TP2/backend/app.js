const express= require('express');
const path= require('path');
const App= express();
const express_layout= require('express-ejs-layouts');


App.set( 'view engine', 'ejs' );
App.set( 'views', path.join(__dirname, 'views') );
App.use( express.static(path.join(__dirname, 'public')) );
App.use( express_layout );
App.use( express.json() );
App.use( express.urlencoded({extended:false}) );
App.set( 'layout', './layouts/layout');

App.use( (req, res, next)=> {
	const NOW= new Date().toDateString();
	console.log( `${NOW}: ${req.method}`)
	next();
});

const HomeRouter= require( './routes/home_router.js');
App.use( '/', HomeRouter );

const SpecificRouter= require( './routes/specify_router.js');
App.use( '/get', SpecificRouter );

module.exports= App;