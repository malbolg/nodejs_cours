const fspromise= require('fs/promises');
const path= require('path');

let jokes= undefined;
fspromise.readFile(path.join(__dirname, '../data/jokes.json'))
	.then((data)=>{
		jokes= JSON.parse(data);
	})
	.catch((err)=>{
		console.log(err)
	});

module.exports.getSpecific = (req, res) => {
	let joke_found= undefined;
	for( let joke_wrapper of jokes ) {
		if( joke_wrapper["id"] === req.params.id ) {
			joke_found= joke_wrapper;
			break;
		}
	}
	console.log(joke_found);
	res.render( './pages/specific_joke.ejs', {data: joke_found});
};

module.exports.home = (req, res) => {
	res.redirect("/get/" + jokes[Math.floor(jokes.length*Math.random())]["id"]);
};
