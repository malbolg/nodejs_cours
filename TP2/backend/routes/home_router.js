const express= require('express');
const Router= express.Router();

const Controller= require( '../controllers/home_controller')
Router.get( '/', Controller.home );
module.exports= Router;