const express= require('express');
const Router= express.Router();

const Controller= require( '../controllers/specify_controller')
Router.get( '/', Controller.home );
Router.get( '/:id', Controller.getSpecific );

module.exports= Router;