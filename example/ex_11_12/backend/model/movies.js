const mongoose= require('mongoose');

// noinspection JSValidateTypes
const movieSchema= mongoose.Schema({
	title: {type: String, required: true},
	synopsis: {type: String, default: "no synopsis available yet"}
});

module.exports = mongoose.model('moviesdb', movieSchema, process.env.COLLECTION_NAME );
