const express = require('express');
const Router = express.Router();
const Controller= require('../controllers/moviesController')

Router.get( '/', Controller.list );
Router.get( '/create', Controller.create );
Router.get( '/delete', Controller.del );
Router.get( '/read', Controller.read );
Router.get( '/update', Controller.update );

module.exports = Router;