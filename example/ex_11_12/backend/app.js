require('dotenv').config({path: './.env'});
const express= require('express');
const path= require('path');
require( './controllers/mongoose');
const fspromise= require('fs/promises');
const App= express();
const express_layout= require('express-ejs-layouts');

App.set( 'view engine', 'ejs' );
App.set( 'views', path.join(__dirname, 'views') );
App.use( express.static(path.join(__dirname, 'public')) );
App.use( express_layout );
App.set( 'layout', './layouts/layout');
App.use( express.json() ); // POST request lib
App.use( express.urlencoded({extended:false}) );




App.use( (req, res, next)=> {
	const NOW= new Date().toDateString();
	console.log( `${NOW}: ${req.method}`)
	next();
});


const homeRouter= require('./routes/homeRouter.js');
App.use( '/', homeRouter );

const movieRouter= require('./routes/moviesRouter.js');
App.use( "/movies", movieRouter );

module.exports= App;