const Movie= require('../model/movies.js');

module.exports.list = (req, res)=> {
	Movie.find()
		.sort({title: 'asc'} )
		.then( (movies)=> {
			console.log(movies)
			res.render('./pages/moviesList', {movies});
		})
		.catch( (err)=> { req.status(400).send(err); });
}

module.exports.create = (req, res)=> {
	Movie.create({title: "House"})
		.then(()=> {
			res.redirect('/movies');
		})
		.catch((err)=> {
			res.status(400).send(err);
		});
}

module.exports.del = (req, res)=> {
	Movie.findOneAndDelete({title: "House"})
		.then(()=> {
			res.redirect('/movies');
		})
		.catch((err)=> {
			res.status(400).send(err);
		});
}

module.exports.read = (req, res)=> {
	Movie.findOne({title:"House"})
		.then( (movie)=> {
			if( movie == null ) res.redirect('/movies');
			res.render( './pages/moviesList', {movies: [movie]})
		})
		.catch((err)=> {
			res.status(400).send(err);
		});
}

module.exports.update = (req, res)=> {
	Movie.updateMany({title:"House"}, {synopsis: "test"})
		.then(()=>{
			res.redirect('/movies');
		})
		.catch((err)=> {
			res.status(400).send(err);
		});
}