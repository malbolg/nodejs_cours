const mongoose= require('mongoose');
const mongoDB= `mongodb+srv://${process.env.TOKEN}@cluster0.778b4.mongodb.net/${process.env.DB_NAME}?retryWrites=true&w=majority`

// noinspection JSCheckFunctionSignatures, JSVoidFunctionReturnValueUsed, JSUnresolvedFunction
mongoose.connect(mongoDB, {useUnifiedTopology: true})
	.then(()=>{
		console.log("\033[32m[MongoDB] started!\033[0m");
	})
	.catch((err)=> {
		console.log("\033[31m[MongoDB] failed!\033[0m");
		}
	);


const DB= mongoose.connection;
DB.on('error', console.error.bind( '\033[31mMongo connection error:\033[0m'))