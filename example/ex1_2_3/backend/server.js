const PORT= process.env.PORT || 3000;
const HTTP= require('http');
const APP= require('./app');

const SERVER= HTTP.createServer(APP)

SERVER.listen(PORT,()=>{
	console.log(`server.js@http://127.0.01:${PORT}/`);
})
