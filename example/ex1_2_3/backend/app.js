const express= require('express');
const PATH= require('path');
const App= express();
const expressLayouts= require('express-ejs-layouts');

App.use( express.static(PATH.join(__dirname, 'public')) );
App.set( 'view engine', 'ejs');
App.use(expressLayouts);
App.set( 'layout', 'layouts/layout' );
App.set( 'views', PATH.join(__dirname, 'view'));

App.use( (req, res, next)=> {
	const NOW= new Date().toDateString();
	console.log( `${NOW}: ${req.method}`)
	next();
});

App.use( (req, res, next)=> {
	res.statusCode= 200;
	res.setHeader( 'Content-type', 'text/html;charset=UTF-8' );
	res.render('pages/home', {user: 'greg'});
});

module.exports= App;
