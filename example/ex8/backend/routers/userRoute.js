const express= require( 'express' );
const Router= express.Router();

const Controller= require( '../controllers/userController.js')
Router.get('/:id', Controller.home);

module.exports= Router;