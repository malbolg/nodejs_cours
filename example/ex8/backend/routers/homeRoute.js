const express= require( 'express' );
const Router= express.Router();

const Controller= require( '../controllers/homeController.js')
Router.get('/', Controller.home);

module.exports= Router;