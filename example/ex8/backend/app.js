const express= require('express');
const path= require( 'path' );
const ejsLayouts= require( 'express-ejs-layouts');
const App= express();

App.set( 'view engine', 'ejs' );
App.use( ejsLayouts );
App.set( 'public', path.join( __dirname, 'public') );
App.use( express.static(path.join(__dirname, 'public')) );
App.set( 'layout', './layouts/main.ejs' );

const homeRoute= require( './routers/homeRoute.js');
App.use( '/', homeRoute );

const userRoute= require( './routers/userRoute.js');
App.use( "/user", userRoute );

module.exports= App;