const EXPR= require('express');
const PATH= require('path');

const APP= EXPR();

APP.use( (req, res, next)=> {
	const NOW= new Date().toDateString();
	console.log( `${NOW}: ${req.method}`)
	next();
});

APP.get("/", (req, res)=> {
	res.send('<h1>homepage</h1>');
});

APP.get("/about", (req, res)=> {
	res.send('<h1>A propos de</h1>');
});


APP.get('*', (req, res) => {
	// res.statusCode= 404;
	// res.send( '<h1>404: not found</h1>');
	res.redirect('/');
});

module.exports= APP;
