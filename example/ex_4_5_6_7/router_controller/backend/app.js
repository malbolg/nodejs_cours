const express= require('express');
const path= require('path');
const ejsLayouts= require('express-ejs-layouts');
const App= express();

App.use( (req, res, next)=> {
	const NOW= new Date().toDateString();
	console.log( `${NOW}: ${req.method}`)
	next();
});

App.set( 'view engine', 'ejs' );
App.set( 'views', path.join(__dirname, 'views') );
App.use( ejsLayouts );
App.set( 'layout', './layouts/main');

const homeRouter= require( './routes/homeRouter.js');
App.use( '/', homeRouter );

const aboutRouter= require( './routes/aboutRouter.js');
App.use( "/about", aboutRouter);


App.use( '*', (req,res)=> {
	res.statusCode= 404;
	res.send( "<h1>404</h1>");
});

module.exports= App;