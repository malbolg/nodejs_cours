const express= require('express');
const Router= express.Router();
const Controller= require( '../controllers/homeController')

Router.get( '/', Controller.home );

module.exports= Router;
