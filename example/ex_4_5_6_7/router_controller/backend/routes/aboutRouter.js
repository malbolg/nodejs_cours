const express= require('express');
const Router= express.Router();
const Controller= require( '../controllers/aboutController')

Router.get( '/', Controller.home );

module.exports= Router;