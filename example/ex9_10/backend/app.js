const express= require('express');
const path= require( 'path' );
const ejsLayouts= require( 'express-ejs-layouts');
const App= express();

App.set( 'view engine', 'ejs' );
App.use( ejsLayouts );
App.use( express.json() );
App.use( express.urlencoded({extended: false}) );
App.set( 'public', path.join( __dirname, 'public') );
App.use( express.static(path.join(__dirname, 'public')) );
App.set( 'layout', './layouts/main.ejs' );

const homeRoute= require( './routers/homeRoute.js');
App.use( '/', homeRoute );

const postRoute= require( './routers/postRoute.js');
App.use( '/post', postRoute );
module.exports= App;