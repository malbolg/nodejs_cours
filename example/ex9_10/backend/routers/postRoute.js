const express= require( 'express' );
const Router= express.Router();
const Controller= require( '../controllers/postController.js')

Router.get('/', Controller.getx);
Router.post('/', Controller.postx);

module.exports= Router;