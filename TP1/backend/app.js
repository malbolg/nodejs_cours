const express= require('express');
const path= require('path');
const App= express();
const express_layout= require('express-ejs-layouts');


App.set( 'view engine', 'ejs' );
App.set( 'views', path.join(__dirname, 'views') );
App.use( express.static(path.join(__dirname, 'public')) );
App.use( express_layout );
App.set( 'layout', './layouts/layout');

App.use( (req, res, next)=> {
	const NOW= new Date().toDateString();
	console.log( `${NOW}: ${req.method}`)
	next();
});

const Router= require( './routes/main_router.js');
App.get( '/', Router );

module.exports= App;