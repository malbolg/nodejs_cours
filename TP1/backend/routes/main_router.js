const express= require('express');
const Router= express.Router();

const Controller= require( '../controllers/main_controller')
Router.get( '/', Controller.any );

module.exports= Router;