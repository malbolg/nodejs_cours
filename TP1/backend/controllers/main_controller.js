const fspromise= require('fs/promises');
const path= require('path');

let jokes= undefined;
fspromise.readFile(path.join(__dirname, '../data/jokes.json'))
	.then((data)=>{
		jokes= JSON.parse(data);
	})
	.catch((err)=>{
		console.log(err)
	});

module.exports.any = (req, res) => {
	res.render( './pages/home', {data: jokes});
};
