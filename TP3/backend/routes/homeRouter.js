const express= require('express');
const Router= express.Router();
const Controller= require('../controllers/homepageController')

Router.get( '/', Controller.homepage );
module.exports= Router;