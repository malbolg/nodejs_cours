const express= require('express');
const path= require('path');
const App= express();
const express_layout= require('express-ejs-layouts');


App.set( 'view engine', 'ejs' );
App.set( 'views', path.join(__dirname, 'views') );
App.use( express.static(path.join(__dirname, 'public')) );
App.use( express_layout );
App.set( 'layout', './layouts/layout');

App.use( express.json() ); // POST request lib
App.use( express.urlencoded({extended:false}) );
App.use( (req, res, next)=> {
	const NOW= new Date().toDateString();
	console.log( `${NOW}: ${req.method}`)
	next();
});

const homeRouter= require( './routes/homeRouter.js');
App.use( '/', homeRouter );

const logRouter= require( './routes/logRouter.js');
App.use( '/login', logRouter );



module.exports= App;