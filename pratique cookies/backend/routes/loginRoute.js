const express = require('express');
const Router = express.Router();
const Controller = require('../controllers/mainController')
const Auth= require( '../controllers/auth' );
Router.get('/', Controller.home);
Router.get('/login', Controller.login);
Router.get('/logout', Controller.logout);
Router.get('/admin', Auth.auth, Controller.admin);


module.exports = Router;