module.exports.home = (req, res) => {
	res.render( "./pages/homepage" );
}

const authorisation= require( './auth' );
module.exports.login= (req, res)=> {
	let UID= 0;
	authorisation.createToken(res, UID);
	res.redirect( '/admin' );
}

module.exports.admin = (req, res)=> {
	let uid= req.userID;
	res.render( 'pages/admin', { uid } );
}

module.exports.logout= (req, res)=> {
	res.clearCookie("LOGUID");
	res.redirect( '/' );
}