const jwt = require('jsonwebtoken');

const COOKIE_NAME= 'LOGUID';

module.exports.createToken= (res, uid)=> {
	const token= jwt.sign(
		{uid},
		process.env.SECRET_KEY,
		{expiresIn: process.env.JWT_EXPIRATION }
	)

	res.cookie(COOKIE_NAME, token, {httpOnly: true});
}

module.exports.auth = (req, res, next)=> {
	const token= req.cookies[COOKIE_NAME];
	if( !token )
		return res.sendStatus(403);


	try {
		const payload= jwt.verify(token, process.env.SECRET_KEY);
		req.userID= payload.uid;
		next();
	} catch {
		req.clearCookie( COOKIE_NAME );
		return res.sendStatus(403);
	}
}

module.exports.logout = (req, res)=> {
	res.clearCookie(COOKIE_NAME);
}

