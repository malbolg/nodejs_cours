const express= require('express');
const path= require('path');
const App= express();
const env_r= require('dotenv').config({path: './.env'});
const Cookie_p= require('cookie-parser');

const express_layout= require('express-ejs-layouts');


App.set( 'view engine', 'ejs' );
App.set( 'views', path.join(__dirname, 'views') );
App.use( express.static(path.join(__dirname, 'public')) );
App.use( express_layout );
App.use( Cookie_p() );
App.set( 'layout', './layouts/layout');

//App.use( express.json() ); // POST request lib
//App.use( express.urlencoded({extended:false}) );

App.use( (req, res, next)=> {
	const NOW= new Date().toDateString();
	console.log( `${NOW}: ${req.method}`)
	next();
});

const homeRouter= require( './routes/loginRoute.js');
App.use( '/', homeRouter );


module.exports= App;