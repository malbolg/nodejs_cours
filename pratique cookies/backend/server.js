const PORT= process.env.port || 3000;
const HTTP= require('http');
const APP= require( './app');
const SERVER= HTTP.createServer(APP);

SERVER.listen(PORT, ()=> {
	console.log(`http://127.0.0.1:${PORT}/`);
});
