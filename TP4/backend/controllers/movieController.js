const Movie = require('../model/moviesModel');

module.exports.list = (req, res)=> {
	Movie.find()
		.sort({title: 'asc'} )
		.then( (movies)=> {
			// console.log(movies);
			res.render('./pages/list', {movies});
		})
		.catch( (err)=> { req.status(400).send(err); });
}

module.exports.read = (req, res)=> {
	// console.log(req.params)
	Movie.findById(req.params["_id"])
		.then( (movie)=> {
			console.log(movie)
			if( movie == null ) res.redirect('/movies/list');
			res.render( './pages/showFilm', {movie});
		})
		.catch((err)=> {
			res.status(400).send(err);
		});
}

module.exports.create = (req, res, next)=> {
	Movie.create(
		{
			title: req.body["title"],
			image: '/image/uploads/'+ req.file.filename
		})
		.then(()=> {
			res.redirect('/movies');
		})
		.catch((err)=> {
			res.status(400).send(err);
		});
	console.log(req.file)
	console.log(req.body)
}

module.exports.create_form = (req, res)=> {
	res.render( './pages/create_form')
}

// module.exports.del = (req, res)=> {
// 	Movie.findOneAndDelete({title: "House"})
// 		.then(()=> {
// 			res.redirect('/movies');
// 		})
// 		.catch((err)=> {
// 			res.status(400).send(err);
// 		});
// }
//

// module.exports.update = (req, res)=> {
// 	Movie.updateMany({title:"House"}, {synopsis: "test"})
// 		.then(()=>{
// 			res.redirect('/movies');
// 		})
// 		.catch((err)=> {
// 			res.status(400).send(err);
// 		});
// }