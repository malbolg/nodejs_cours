const mongoose= require('mongoose');
const mongoDB= `mongodb://127.0.0.1:${process.env.DB_PORT}/${process.env.DB_NAME}`

// noinspection JSCheckFunctionSignatures, JSVoidFunctionReturnValueUsed, JSUnresolvedFunction
mongoose.connect(mongoDB, {useUnifiedTopology: true})
	.then(()=>{
		console.log("\033[32m[MongoDB] started!\033[0m");
	})
	.catch((err)=> {
		console.log("\033[31m[MongoDB] failed!\033[0m");
		}
	);


const DB= mongoose.connection;
DB.on('error', console.error.bind( '\033[31mMongo connection error:\033[0m'))