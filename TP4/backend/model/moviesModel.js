const mongoose = require('mongoose');

// noinspection JSValidateTypes
const movieSchema = mongoose.Schema({
	title: {type: String, required: true},
	synopsis: {type: String, default: "No synopsis yet"},
	image: {type: String, default: "../image/movies/missing.png"},
	__v: {type: Number, default: 0}
});

module.exports = mongoose.model(process.env.DB_NAME, movieSchema, process.env.COLLECTION_NAME);
