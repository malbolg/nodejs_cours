const express = require('express');
const Router = express.Router();
const Controller = require('../controllers/movieController')

const multer = require("multer");
const fs = require('fs');
const path = require("path");
const PUBLIC_DIR = "./public";
const UPLOAD_DIR = '/image/uploads/';
const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		const dest = PUBLIC_DIR + UPLOAD_DIR;
		if (!fs.existsSync(dest)) {
			fs.mkdirSync(dest, {recursive: true});
		}
		cb(null, dest);
	},
	filename: function(req, file, cb) {
		cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
	}
})

const upload = multer({storage});

Router.get('/', (req, res)=> {
	res.redirect("/movies/list");
});

Router.get('/list', Controller.list);
Router.get('/get/:_id', Controller.read);
Router.get('/create', Controller.create_form);
Router.post('/create', upload.single('image'), Controller.create);

module.exports = Router;